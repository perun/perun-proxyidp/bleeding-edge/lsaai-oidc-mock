## [2.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.5.2...v2.5.3) (2024-05-30)


### Bug Fixes

* 🐛 Modification of AUD in GA4GH AT modifier ([34d3b11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/34d3b11f0c2d39e117067f986d0da62ee41d9be3))

## [2.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.5.1...v2.5.2) (2024-02-21)


### Bug Fixes

* 🐛 unused param, introduce constants ([9dd97fd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/9dd97fd04f411f3e60d5701fc453231e166aaf07))

## [2.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.5.0...v2.5.1) (2024-02-15)


### Bug Fixes

* 🐛 Use different scope for dynreg deregistration ([f0e335f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/f0e335f8fba05275b019e38cfa362baf692a91a6))

# [2.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.13...v2.5.0) (2024-02-15)


### Features

* 🎸 Delete dynreg client endpoint ([d1240a3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/d1240a3bfae2d863f80c6d9164c85d3ff79f2802))

## [2.4.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.12...v2.4.13) (2024-02-14)


### Bug Fixes

* 🐛 Remove AUD extension from token extra info ([09e2312](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/09e2312c3507e5273ddf1ff0b23361dcb5f5a453))

## [2.4.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.11...v2.4.12) (2024-02-14)


### Bug Fixes

* 🐛 remove addit. info. (aud, resource) from token responses ([47d1cf2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/47d1cf29a266142fe7064e80d6a3dabdfc3052a4))

## [2.4.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.10...v2.4.11) (2024-02-14)


### Bug Fixes

* 🐛 Load correctly AUD extension from token in enhancer ([2b73793](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2b7379340eb88b87a3f82515eff645d5b4ba999c))

## [2.4.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.9...v2.4.10) (2024-02-14)


### Bug Fixes

* 🐛 set correct audiences when refreshing access token ([ccf672e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/ccf672e2ec0a7fbd679f87de8c73629a2ad8f708))

## [2.4.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.8...v2.4.9) (2024-01-24)


### Bug Fixes

* 🐛 def. scopes in token exch if no scope param is passed ([681c6a4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/681c6a4a7fbfceb427402c07b9203672bad63a54))
* 🐛 dynreg mismatched resources out and response fields case ([5f6c6c5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/5f6c6c5ea0df3e611bd2e436dd688fc24ceedf72))

## [2.4.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.7...v2.4.8) (2024-01-23)


### Bug Fixes

* 🐛 client models ([200373e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/200373e995e624123bd80562becc869c33671e12))

## [2.4.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.6...v2.4.7) (2024-01-23)


### Bug Fixes

* 🐛 parsing body in dynreg, JSON keys must be in snakecase ([fabfc85](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/fabfc85ca8468ef79e3ecaedde993888c071f7ed))

## [2.4.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.5...v2.4.6) (2024-01-23)


### Bug Fixes

* 🐛 parsing body in dynreg, JSON keys must be in snakecase ([1c9d9cc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/1c9d9cc21850a52e50f862ca20660a5412b98687))

## [2.4.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.4...v2.4.5) (2024-01-22)


### Bug Fixes

* 🐛 request validation in dynreg ([b49e9dd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/b49e9ddb42f5e90a45d6e012ce7001300f14252f))

## [2.4.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.3...v2.4.4) (2024-01-19)


### Bug Fixes

* 🐛 comparing allowed resourceIds ([f786193](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/f786193053943ad7da64b17ceb16f68794daccce))
* 🐛 enable refresh for clientCredentials grant ([98e9d92](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/98e9d9296ba4b8c01f9929ac81402ddbaaf10244))

## [2.4.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.2...v2.4.3) (2024-01-15)


### Bug Fixes

* 🐛 Fix PKCE conversion from DB ([e752ab1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/e752ab1c420594cbe41e9da1788dd1c743f7ff1b))

## [2.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.1...v2.4.2) (2024-01-11)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.11 ([0b68bda](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0b68bda2c8b14c8c990c0adb12d1bfee5b1a92d0))

## [2.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.4.0...v2.4.1) (2023-12-31)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.10 ([86adc84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/86adc84d84a6eb03196e39ab9d1408248e8dccd8))

# [2.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.11...v2.4.0) (2023-12-27)


### Features

* 🎸 Support for dynamic registration ([b234984](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/b234984d98cc0ba6800c75c90c5d9b28fc5f7a26))

## [2.3.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.10...v2.3.11) (2023-12-27)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.16.1 ([417190c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/417190c43bbd0c294084c4efe9cd3cb60b943c74))

## [2.3.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.9...v2.3.10) (2023-12-21)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.9 ([2098a78](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2098a78a9449f4d5b4ba4082718c38f1374937a2))

## [2.3.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.8...v2.3.9) (2023-12-16)


### Bug Fixes

* **deps:** update eclipse-persistence.version to v2.7.14 ([aebc84b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/aebc84b0545dbaf40a98824aa49cf2abd18792e6))

## [2.3.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.7...v2.3.8) (2023-12-14)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.21 ([d7f0c8d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/d7f0c8d23c7947c152ab6a99be33d58dac5073e5))

## [2.3.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.6...v2.3.7) (2023-12-11)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.37.3 ([7a224bc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/7a224bc965c6d7f96d501d99d369babc6c4e6221))

## [2.3.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.5...v2.3.6) (2023-12-06)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.37.2 ([9895f7e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/9895f7e8325e630538a4e6bc0d96bf2a2920c847))

## [2.3.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.4...v2.3.5) (2023-12-04)


### Bug Fixes

* **deps:** update logback.version to v1.4.14 ([4ea7819](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/4ea7819b9697361f873c599ab08759ecffcb1fc3))

## [2.3.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.3...v2.3.4) (2023-12-01)


### Bug Fixes

* **deps:** update logback.version to v1.4.13 ([829f50a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/829f50a3e97b909028b88f493d203d6f9545e6ff))

## [2.3.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.2...v2.3.3) (2023-11-30)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.4.12 [security] ([edbe7d9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/edbe7d94060c7b34aac9f8ac5e6360274a9fa198))

## [2.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.1...v2.3.2) (2023-11-27)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.16.0 ([df44ae6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/df44ae6f10cfba82fa9c0012b52d6aa06c5e77da))

## [2.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.3.0...v2.3.1) (2023-11-19)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.31 ([6d82bc6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/6d82bc6c67c05385bccf715260bbe2e8f4090ee6))

# [2.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.6...v2.3.0) (2023-11-15)


### Features

* 🎸 Client credentials grant ([0c478d8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0c478d89c6ba5b9997de400f3c66cd6fea0d93db))

## [2.2.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.5...v2.2.6) (2023-11-15)


### Bug Fixes

* 🐛 reading resourceIds in client_credentials grant ([6ab7c6c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/6ab7c6cf7dfe04c0de21b31668bff4f737fd6d35))

## [2.2.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.4...v2.2.5) (2023-11-14)


### Bug Fixes

* **deps:** update dependency com.zaxxer:hikaricp to v5.1.0 ([0b5a957](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0b5a9571dc5f9d248c11ccfbe0feff7a435e2675))

## [2.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.3...v2.2.4) (2023-11-11)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.37.1 ([4a15fc8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/4a15fc85d23cf128f3071ee090584e76d9c3922b))

## [2.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.2...v2.2.3) (2023-10-31)


### Bug Fixes

* **deps:** update dependency com.mysql:mysql-connector-j to v8.2.0 ([7b72a47](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/7b72a47627e4e2ab3044e6356d9435f1dc4e32a7))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.37 ([7949af9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/7949af9b99fc3471532a64f8551e51c7227add8a))

## [2.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.1...v2.2.2) (2023-10-20)


### Bug Fixes

* **deps:** update dependency org.glassfish.jaxb:jaxb-runtime to v2.3.9 ([59e2375](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/59e2375d31ab602153175eea729f0575053ee6d2))

## [2.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.2.0...v2.2.1) (2023-10-19)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.8 ([e862700](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/e8627000bd3cec9f029b6b8f83772102a387eadd))

# [2.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.14...v2.2.0) (2023-10-18)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.3 ([0ee373a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0ee373a0a9a1576fc177157794520ec6620847ee))
* **deps:** update dependency com.google.guava:guava to v32.1.3-jre ([2b833cc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2b833ccedc4be221b2dcfa4d6926bc5f6bd0b888))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.36 ([cf27b93](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/cf27b937e6d67d825906f226070b6a07c61d1c97))


### Features

* 🎸 Support for username_hint to bypass user selection ([faa7cd1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/faa7cd1f133655e29ab8ab928de6949c9fa3717d))

## [2.1.14](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.13...v2.1.14) (2023-10-10)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.35 ([dfe710f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/dfe710fbee77fe337b608c0ad2cb4793b63c985a))

## [2.1.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.12...v2.1.13) (2023-09-23)


### Bug Fixes

* **deps:** update dependency org.projectlombok:lombok to v1.18.30 ([d1c7ebb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/d1c7ebbc4044204d3d2c2b5e1d475d55b00342e1))

## [2.1.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.11...v2.1.12) (2023-09-21)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.7 ([bac55f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/bac55f1f5be5755b3997f6a01844b63c25dcca6c))

## [2.1.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.10...v2.1.11) (2023-09-17)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.32 ([bd1acdf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/bd1acdfc8ad5987493e8845de1c754a6cfa6ace9))

## [2.1.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.9...v2.1.10) (2023-09-14)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.30 ([9bb1fe3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/9bb1fe3201105d64208df23acfe9c4fcb3c7b93b))

## [2.1.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.8...v2.1.9) (2023-09-07)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.20.1 ([4b8d212](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/4b8d2121abba0a350fb0fe7a1cd93d463b988466))

## [2.1.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.7...v2.1.8) (2023-09-06)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.9 ([624a00e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/624a00e8f1bc8da726848d5a46a605d5e0adbf2c))

## [2.1.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.6...v2.1.7) (2023-08-24)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.6 ([17d3658](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/17d36583dced7d79d369e0ebfed37db5861a58fc))

## [2.1.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.5...v2.1.6) (2023-08-19)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.20 ([2c5e28a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2c5e28adc53ea1f5b4633a7e8f099fdd7232e962))

## [2.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.4...v2.1.5) (2023-08-12)


### Bug Fixes

* **deps:** update logback.version to v1.4.11 ([242fe68](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/242fe68354991200b2b79897b3801eda29038da4))

## [2.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.3...v2.1.4) (2023-08-12)


### Bug Fixes

* **deps:** update logback.version to v1.4.10 ([2b05642](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2b056424aaed61909c29e0b1ddb717876a9237b0))

## [2.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.2...v2.1.3) (2023-08-07)


### Bug Fixes

* **deps:** update logback.version to v1.4.9 ([3483697](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/34836975f61fbe00eae383df5aaba814958f1d19))

## [2.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.1...v2.1.2) (2023-08-03)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32.1.2-jre ([5b26bf7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/5b26bf7757ddc8f10c1e8f72bc1263dadbb18477))

## [2.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.1.0...v2.1.1) (2023-07-30)


### Bug Fixes

* **deps:** update eclipse-persistence.version to v2.7.13 ([904b308](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/904b308ec69cf546cc9e7069a2ed15dfd8b58f91))

# [2.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.12...v2.1.0) (2023-07-21)


### Features

* 🎸 Update to latest version according to production ([2cdcb91](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2cdcb91a898fe2a9fa76a76443fee25ac5fea2c1))

## [2.0.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.11...v2.0.12) (2023-07-20)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.5 ([0dccc98](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0dccc98c3a5234670b47ea1e84fc5b23024607c9))

## [2.0.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.10...v2.0.11) (2023-07-19)


### Bug Fixes

* **deps:** update dependency com.mysql:mysql-connector-j to v8.1.0 ([963b517](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/963b5173004db63d5fd9bc5efd0436fea3ac5476))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.29 ([cf4e1c7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/cf4e1c77dfb2edbbe7fa4c11ef35e1e8b247beb5))

## [2.0.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.9...v2.0.10) (2023-07-19)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32.1.1-jre ([f3416c8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/f3416c8af44e50413a96bb3e2283ed01ca963483))

## [2.0.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.8...v2.0.9) (2023-06-22)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.4 ([f398bb3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/f398bb33312be9307b106890599764b0f7340c50))

## [2.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.7...v2.0.8) (2023-06-18)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.28 ([8a5776d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/8a5776d75ae17a4dfbf3902bd2cfbfbf3343b090))

## [2.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.6...v2.0.7) (2023-06-16)


### Bug Fixes

* **deps:** update logback.version to v1.4.8 ([6874ea5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/6874ea57643ba0ba2d2b91bf830b58448b1af2c1))

## [2.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.5...v2.0.6) (2023-06-12)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32.0.1-jre ([1482aea](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/1482aeae790a083988f70b792418b07bacecfe25))

## [2.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.4...v2.0.5) (2023-06-07)


### Bug Fixes

* 🐛 Fix discovery endpoint algs JSON ([beb7d63](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/beb7d63fac76fadf72ded5415971853713711c95))

## [2.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.3...v2.0.4) (2023-06-03)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.2 ([2f11ff9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2f11ff99c515c194b5c0c6e8fa529198c35bfe44))

## [2.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.2...v2.0.3) (2023-05-30)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32 ([adb77aa](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/adb77aa86abbe969cd93fd1071bbd0dc2aa3180d))

## [2.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.1...v2.0.2) (2023-05-27)


### Bug Fixes

* **deps:** update dependency org.projectlombok:lombok to v1.18.28 ([1839f2c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/1839f2cfa61f68208545e7ecf75a6a86dbbca233))

## [2.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v2.0.0...v2.0.1) (2023-05-20)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.1 ([cf0eb1f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/cf0eb1ffe5f964e702a468cd299082592870c097))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v1.0.4...v2.0.0) (2023-05-11)


### Bug Fixes

* 🐛 Fix discovery endpoint contents ([2fcff6d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/2fcff6df0e1f7ee1cff56f1dac30900fdb350bfd))
* 🐛 Update naming of eduPerson and voPerson ([7085900](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/7085900db060a0f9314ac2bd8a0c6208b68f2d3e))


### BREAKING CHANGES

* 🧨 EduPersonScopedAffiliation is an array when configuring

## [1.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v1.0.3...v1.0.4) (2023-05-03)


### Bug Fixes

* 🐛 Fix login issues and add logging ([6213364](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/621336431943a40625e589c601b94bee3d2a63b1))

## [1.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v1.0.2...v1.0.3) (2023-05-03)


### Bug Fixes

* 🐛 Fix login issues and add logging ([a86ba0c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/a86ba0c2f951c60e222c0ea8235383ad9f997f69))

## [1.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v1.0.1...v1.0.2) (2023-05-03)


### Bug Fixes

* 🐛 Include JSP tags directory ([5e3f048](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/5e3f0482b628fb11a279a102e90ef20997ed32be))

## [1.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/compare/v1.0.0...v1.0.1) (2023-05-03)


### Bug Fixes

* 🐛 Fix logging and small bugs ([9548dae](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/9548dae6a63d5ccfb3360b34963be9e90d294982))

# 1.0.0 (2023-05-02)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.0 ([0109b46](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/0109b469149f48d5101da4a21058216e97defdc1))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.27 ([283dc39](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/283dc39eca6383026e1108acf307bc8c1052b558))
* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.3 ([353ecae](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/353ecae25bd229de461dec5461beef169264a07a))
* **deps:** update logback.version to v1.4.7 ([c912e30](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/c912e30efe30b5dac20156660fee52ffebaee859))


### Features

* 🎸 Initial version ([a89700d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock/commit/a89700d8d5fc3fd1c6f7fed026e2b99f20f98cdb))

/*******************************************************************************
 * Copyright 2018 The MIT Internet Trust Consortium
 *
 * Portions copyright 2011-2013 The MITRE Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/**
 *
 */
package cz.muni.ics.openid.connect.assertion;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.Collection;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class TokenExchangeAuthenticationToken extends AbstractAuthenticationToken {
	private String subjectToken;

	private String subjectTokenType;

	private String scope;

	private String resource;

	private String audience;

	private String actorToken;

	private String actorTokenType;

	private String requestedTokenType;

	private ClientDetails client;

	public TokenExchangeAuthenticationToken() {
		this(null);
	}

	public TokenExchangeAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		setAuthenticated(true);
	}


	@Override
	public Object getCredentials() {
		return subjectToken;
	}


	@Override
	public Object getPrincipal() {
		return this;
	}

}

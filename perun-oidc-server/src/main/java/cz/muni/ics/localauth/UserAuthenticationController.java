package cz.muni.ics.localauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class UserAuthenticationController {

    public static final String PATH_LOGIN = "/login";

    public static final String PATH_LOGOUT = "/logout";

    public static final String PATH_LOGOUT_SUCCESS = "/logout/success";

    private final SelectedUserDetailsRepository userDetailsRepository;

    @Autowired
    public UserAuthenticationController(SelectedUserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @GetMapping(value = PATH_LOGIN)
    public String login(Map<String, Object> modelMap)  {
        modelMap.put("users", userDetailsRepository.getAllUsers());
        return "login";
    }

    @GetMapping(value = PATH_LOGOUT)
    public String logout()  {
        return "logout";
    }

}

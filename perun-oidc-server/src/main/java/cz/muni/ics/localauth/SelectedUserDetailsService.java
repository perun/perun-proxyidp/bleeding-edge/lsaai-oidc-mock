package cz.muni.ics.localauth;

import lombok.NonNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class SelectedUserDetailsService implements UserDetailsService  {

    private final SelectedUserDetailsRepository userDetailsRepository;

    public SelectedUserDetailsService(@NonNull SelectedUserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public UserDetails loadUserByUsername(@NonNull String username) throws UsernameNotFoundException {
        SelectedUserDetails userDetails = userDetailsRepository.getByUsername(username);
        if (userDetails == null) {
            throw new UsernameNotFoundException("User with " + username + " not found");
        }
        return userDetails;
    }



}

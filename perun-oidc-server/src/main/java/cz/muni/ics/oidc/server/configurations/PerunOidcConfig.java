package cz.muni.ics.oidc.server.configurations;

import cz.muni.ics.openid.connect.config.ConfigurationPropertiesBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import javax.servlet.ServletContext;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Configuration of OIDC server in context of Perun.
 * Logs some interesting facts.
 *
 * @author Martin Kuba <makub@ics.muni.cz>
 */
@Slf4j
@Getter
@Setter
public class PerunOidcConfig {

	private static final String OIDC_POM_FILE = "/META-INF/maven/cz.muni.ics/perun-oidc-server-webapp/pom.properties";

	private final Map<String, String> languageMap = Map.of("en", "English");

	private ConfigurationPropertiesBean configBean;

	private String baseURL;

	private Set<String> idTokenScopes;

	private String ga4ghTokenExchangeBrokerUrl;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private Properties coreProperties;

	public Set<String> getAvailableLangs() {
		return languageMap.keySet();
	}

	//called when all beans are initialized, but twice, once for root context and once for spring-servlet
	@EventListener
	public void handleContextRefresh(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			//log info
			log.info("Mock AAI initialized");
			log.info("Well-known URL: {}.well-known/openid-configuration", configBean.getIssuer(true));
		}
	}

}

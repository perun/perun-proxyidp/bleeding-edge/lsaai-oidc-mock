package cz.muni.ics.oidc.server.ga4gh;


import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.oidc.server.configurations.Ga4ghBrokerConfig;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Class producing GA4GH Passport claim.
 * @author Dominik Baranek <baranek@ics.muni.cz>
 */
@Slf4j
public class Ga4ghApiClaimSource {

    public static final String GA4GH_SCOPE = "ga4gh_passport_v1";

    public static final String GA4GH_CLAIM = "ga4gh_passport_v1";

    private static final String SUB = "sub";

    private final RestTemplate restTemplate;

    private final String endpoint;

    private final String authUsername;

    private final String authPassword;

    public Ga4ghApiClaimSource(@NonNull Ga4ghBrokerConfig config) {
        this.endpoint = (config.getUrl().endsWith("/") ? config.getUrl() : config.getUrl() + '/') + "1.0/{sub}/";
        this.authUsername = config.getUsername();
        this.authPassword = config.getPassword();
        this.restTemplate = new RestTemplate(getClientHttpRequestFactory());
    }

    public Set<String> produceValue(String sub) {
        try {
            try {
                log.debug("loading via API call; params - endpoint '{}', username (auth) '{}', userIdParam '{}'; user '{}'",
                        endpoint, authUsername, sub, sub);
                JsonNode result = restTemplate.getForObject(
                        endpoint, JsonNode.class, Collections.singletonMap(SUB, sub));
                log.debug("loaded Passport(user: {}) - '{}'", sub, result);
                if (result != null && result.isArray() && ! result.isEmpty()) {
                    Set<String> visas = new HashSet<>();
                    for (JsonNode visa: result) {
                        visas.add(visa.asText());
                    }
                    return visas;
                }
            } catch (RestClientException e) {
                log.warn("API call for user {} responded an error: {}", sub, e.getMessage());
            }
            return new HashSet<>();
        } catch (Exception e) {
            log.warn("caught exception {}", e);
        }
        return new HashSet<>();
    }

    private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClient());

        return clientHttpRequestFactory;
    }

    private HttpClient httpClient() {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(authUsername, authPassword));

        return HttpClientBuilder
                .create()
                .setDefaultCredentialsProvider(credentialsProvider)
                .build();
    }

}

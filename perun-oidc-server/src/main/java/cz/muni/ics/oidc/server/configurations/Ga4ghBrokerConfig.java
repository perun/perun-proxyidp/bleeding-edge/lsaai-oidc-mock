package cz.muni.ics.oidc.server.configurations;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Ga4ghBrokerConfig {

    private final String username;

    private final String password;

    private final String url;

}

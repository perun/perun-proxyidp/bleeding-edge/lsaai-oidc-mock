/*******************************************************************************
 * Copyright 2018 The MIT Internet Trust Consortium
 *
 * Portions copyright 2011-2013 The MITRE Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/**
 *
 */
package cz.muni.ics.oauth2.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import cz.muni.ics.oauth2.model.enums.AuthMethod;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.StringUtils;

import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Dominik F. Bucik &lt;bucik@ics.muni.cz&gt;
 */
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MockOidcClientDetailsEntity implements ClientDetails {

	@JsonAlias("client-name")
	private String clientName;

	@JsonAlias("client-id")
	private String clientId = null;

	@JsonAlias("client-secret")
	private String clientSecret = null;

	@JsonAlias("redirect-uris")
	private Set<String> redirectUris = new HashSet<>();

	@JsonAlias("token-endpoint-auth-method")
	private AuthMethod tokenEndpointAuthMethod = AuthMethod.SECRET_BASIC;

	@JsonAlias("scope")
	private Set<String> scope = new HashSet<>();

	@JsonAlias("grant-types")
	private Set<String> grantTypes = new HashSet<>();

	@JsonAlias("post-logout-redirect-uris")
	private Set<String> postLogoutRedirectUris = new HashSet<>();

	@JsonAlias("access-token-validity-seconds")
	private int accessTokenValiditySeconds = 3600;

	@JsonAlias("refresh-token-validity-seconds")
	private int refreshTokenValiditySeconds = 7200;

	@JsonAlias("id-token-validity-seconds")
	private int idTokenValiditySeconds = 3600;

	@JsonAlias("device-code-validity-seconds")
	private int deviceCodeValiditySeconds = 300;

	@JsonAlias("code-challenge-method")
	private String codeChallengeMethod = null;

	@JsonAlias("resource-ids")
	private Set<String> resourceIds = new HashSet<>();

	@JsonIgnore
	private String parentClientId = null;

	@JsonIgnore
	private boolean dynamicallyRegistered = false;

	@Override
	public String getClientId() {
		return clientId;
	}

	@Override
	public String getClientSecret() {
		return clientSecret;
	}

	@Override
	public Set<String> getScope() {
		return scope;
	}

	@Override
	public Set<GrantedAuthority> getAuthorities() {
		return Set.of(new SimpleGrantedAuthority("ROLE_CLIENT"));
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return refreshTokenValiditySeconds;
	}

	@Override
	public boolean isAutoApprove(String scope) {
		return false;
	}

	@Override
	@Transient
	public boolean isSecretRequired() {
		return getTokenEndpointAuthMethod() != null &&
				(getTokenEndpointAuthMethod().equals(AuthMethod.SECRET_BASIC) ||
						getTokenEndpointAuthMethod().equals(AuthMethod.SECRET_POST) ||
						getTokenEndpointAuthMethod().equals(AuthMethod.SECRET_JWT));
	}

	@Override
	@Transient
	public boolean isScoped() {
		return getScope() != null && !getScope().isEmpty();
	}

	@Override
	@Transient
	public Set<String> getAuthorizedGrantTypes() {
		return getGrantTypes();
	}

	@Override
	@Transient
	public Set<String> getRegisteredRedirectUri() {
		return getRedirectUris();
	}

	@Override
	@Transient
	public Map<String, Object> getAdditionalInformation() {
		return Map.of();
	}

	@Override
	public Set<String> getResourceIds() {
		return resourceIds;
	}

	public void setTokenEndpointAuthMethod(AuthMethod method) {
		if (method == null) {
			throw new IllegalArgumentException();
		}
		this.tokenEndpointAuthMethod = method;
	}

	public void setTokenEndpointAuthMethod(String method) {
		if (!StringUtils.hasText(method)) {
			throw new IllegalArgumentException();
		}
		this.tokenEndpointAuthMethod = AuthMethod.getByValue(method);
	}

	public PKCEAlgorithm getCodeChallengeMethod() {
		if (!StringUtils.hasText(codeChallengeMethod)) {
			return null;
		}
		return PKCEAlgorithm.getByAlgorithm(codeChallengeMethod);
	}

	public void setClientName(String clientName) {
		if (!StringUtils.hasText(clientName)) {
			throw new IllegalArgumentException();
		}
		this.clientName = clientName;
	}

	public void setClientId(String clientId) {
		if (!StringUtils.hasText(clientId)) {
			throw new IllegalArgumentException();
		}
		this.clientId = clientId;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public void setRedirectUris(Set<String> redirectUris) {
		if (redirectUris == null) {
			redirectUris = new HashSet<>();
		}
		this.redirectUris = redirectUris;
	}

	public void setScope(Set<String> scope) {
		if (scope == null) {
			scope = new HashSet<>();
		}
		this.scope = scope;
	}

	public void setGrantTypes(Set<String> grantTypes) {
		if (grantTypes == null) {
			grantTypes = new HashSet<>();
		}
		this.grantTypes = grantTypes;
	}

	public void setPostLogoutRedirectUris(Set<String> postLogoutRedirectUris) {
		if (postLogoutRedirectUris == null) {
			postLogoutRedirectUris = new HashSet<>();
		}
		this.postLogoutRedirectUris = postLogoutRedirectUris;
	}

	public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
		if (accessTokenValiditySeconds == null) {
			accessTokenValiditySeconds = 3600;
		}
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
	}

	public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
		if (refreshTokenValiditySeconds == null || refreshTokenValiditySeconds < 0 ) {
			return;
		}
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
	}

	public void setIdTokenValiditySeconds(Integer idTokenValiditySeconds) {
		if (idTokenValiditySeconds == null || idTokenValiditySeconds < 0 ) {
			return;
		}
		this.idTokenValiditySeconds = idTokenValiditySeconds;
	}

	public void setDeviceCodeValiditySeconds(Integer deviceCodeValiditySeconds) {
		if (deviceCodeValiditySeconds == null || deviceCodeValiditySeconds < 0 ) {
			return;
		}
		this.deviceCodeValiditySeconds = deviceCodeValiditySeconds;
	}

	public void setCodeChallengeMethod(String codeChallengeMethod) {
		if (!StringUtils.hasText(codeChallengeMethod)) {
			codeChallengeMethod = PKCEAlgorithm.NONE.getAlgorithm().getName();
		}

		if (!PKCEAlgorithm.isSupported(codeChallengeMethod)) {
			throw new IllegalArgumentException("Unsupported code challenge method: " + codeChallengeMethod);
		}
		PKCEAlgorithm pkce = PKCEAlgorithm.getByAlgorithmName(codeChallengeMethod);
		this.codeChallengeMethod = pkce.getAlgorithm().getName();
	}

	public void setResourceIds(Set<String> resourceIds) {
		if (resourceIds == null) {
			resourceIds = new HashSet<>();
		}
		this.resourceIds = resourceIds;
	}

	public void makeClientDynamicallyRegistered(String parentClientId) {
		if (!StringUtils.hasText(parentClientId)) {
			throw new IllegalArgumentException("When making client dynamically registered, parentClientId must be provided");
		}
		this.parentClientId = parentClientId;
		this.dynamicallyRegistered = true;
	}
}

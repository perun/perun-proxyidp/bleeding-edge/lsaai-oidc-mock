package cz.muni.ics.oauth2.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum AuthMethod {
    SECRET_POST("client_secret_post"),
    SECRET_BASIC("client_secret_basic"),
    SECRET_JWT("client_secret_jwt"),
    PRIVATE_KEY("private_key_jwt"),
    NONE("none");

    private final String value;

    // map to aid reverse lookup
    private static final Map<String, AuthMethod> lookup = new HashMap<>();
    static {
        for (AuthMethod a : AuthMethod.values()) {
            lookup.put(a.getValue(), a);
        }
    }

    public static AuthMethod getByValue(String value) {
        if (!isSupported(value)) {
            throw new IllegalArgumentException("Unrecognized value: " + value);
        }
        value = value.toLowerCase();
        return lookup.get(value);
    }

    public static boolean isSupported(String authMethod) {
        if (authMethod == null) {
            throw new IllegalArgumentException("authMethod must not be null");
        }
        authMethod = authMethod.toLowerCase();
        return lookup.containsKey(authMethod);
    }

}

package cz.muni.ics.oauth2.service;

import java.util.Set;

public interface ScopeService {

    Set<String> getScopes();
}

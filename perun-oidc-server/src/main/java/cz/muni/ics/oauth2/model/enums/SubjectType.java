package cz.muni.ics.oauth2.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum SubjectType {

    PAIRWISE("pairwise"), PUBLIC("public");

    private final String value;

    // map to aid reverse lookup
    private static final Map<String, SubjectType> lookup = new HashMap<>();
    static {
        for (SubjectType u : SubjectType.values()) {
            lookup.put(u.getValue(), u);
        }
    }

    public static SubjectType getByValue(String value) {
        if (!isSupported(value)) {
            throw new IllegalArgumentException("Unrecognized value: " + value);
        }
        value = value.toLowerCase();
        return lookup.get(value);
    }

    public static boolean isSupported(String subjectType) {
        if (subjectType == null) {
            throw new IllegalArgumentException("subjectType must not be null");
        }
        subjectType = subjectType.toLowerCase();
        return lookup.containsKey(subjectType);
    }
}

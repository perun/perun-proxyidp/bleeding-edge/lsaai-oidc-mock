<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ls" tagdir="/WEB-INF/tags/lsaai" %>

<ls:header />

    <div id="head">
        <h1><spring:message code="login_header"/></h1>
    </div>
    <div>
        <form action="" method="POST">
            <select name="username" class="form-control">
                <c:forEach var="userEntry" items="${users}">
                    <option value="${userEntry.username}">${userEntry.name}</option>
                </c:forEach>
            </select>
            <input type="hidden" name="password" value="">
            <input type="submit" value="Login" class="btn btn-block btn-primary mt-2">
        </form>
    </div>

<ls:footer />
